import React from 'react';
import PagesComponent from './pages/pages.container';
import '../theme/index.scss';

function App() {
  return (
    <div>
      <PagesComponent />
    </div>
  );
}

export default App;
