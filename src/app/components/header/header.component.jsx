import React, { Component } from 'react';
import { BellOutlined, LogoutOutlined } from '@ant-design/icons';

class HeaderComponent extends Component {
  logout = () => {
    localStorage.clear();
    window.location.reload();
  };
  render() {
    console.log(this.props);
    return (
      <>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <h5 style={{ fontWeight: 'bold', textTransform: 'uppercase' }}>
            {this.props.commerce ? this.props.commerce.name : null}
          </h5>

          <div style={{ display: 'flex' }}>
            <BellOutlined style={{ fontSize: 25 }} />
            <LogoutOutlined style={{ fontSize: 25 }} onClick={this.logout} />
          </div>
        </div>
      </>
    );
  }
}

export default HeaderComponent;
