import {
  connect
} from 'react-redux';
import HeaderComponent from './header.component';

const mapStateToProps = state => ({
  commerce: state.auth.commerce
});

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderComponent);