import {
  notification
} from 'antd';

const showNotification = (type, message, description) => {
  notification[type]({
    message: message,
    description: description,
    placement: 'bottomRight'
  });
};

export default {
  showNotification
}