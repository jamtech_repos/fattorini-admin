import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import {
  HomeOutlined,
  ShoppingCartOutlined,
  TeamOutlined,
  ShoppingOutlined,
  ShopOutlined,
  ProfileOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import './sidemenu.scss';
import Logo from '../../../assets/svg/fattorinisvg.svg';

const { Sider } = Layout;

class SideMenuComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      role: '',
    };
  }
  componentDidMount() {
    this.getRole();
  }
  onCollapse = (collapsed) => {
    this.setState({ collapsed: collapsed });
  };
  getRole = () => {
    let role = localStorage.getItem('role');
    this.setState({ role: role });
  };
  render() {
    return (
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
      >
        <div className='logo'>
          {!this.state.collapsed ? (
            <img style={{ width: '80%' }} src={Logo} alt='logo fattorini' />
          ) : (
            <h3>F</h3>
          )}
        </div>
        <Menu
          theme='default'
          defaultSelectedKeys={[window.location.pathname]}
          mode='inline'
        >
          <Menu.Item key='/admin/dashboard' icon={<HomeOutlined />}>
            <Link to='/admin/dashboard'>Dashboard</Link>
          </Menu.Item>
          {this.state.role === 'A' ? (
            <Menu.Item key='/admin/categorias' icon={<ProfileOutlined />}>
              <Link to='/admin/categorias'>Categorias</Link>
            </Menu.Item>
          ) : null}
          {this.state.role === 'A' ? (
            <Menu.Item key='/admin/comercios' icon={<ShopOutlined />}>
              <Link to='/admin/comercios'>Comercios</Link>
            </Menu.Item>
          ) : null}

          {this.state.role === 'CA' ? (
            <Menu.Item key='/admin/menuproductos' icon={<ShoppingOutlined />}>
              <Link to='/admin/menuproductos'>Menu </Link>
            </Menu.Item>
          ) : null}
          {this.state.role === 'CA' ? (
            <Menu.Item key='/admin/productos' icon={<ShoppingOutlined />}>
              <Link to='/admin/productos'>Productos</Link>
            </Menu.Item>
          ) : null}

          <Menu.Item key='/admin/pedidos' icon={<ShoppingCartOutlined />}>
            <Link to='/admin/pedidos'>Pedidos</Link>
          </Menu.Item>
          <Menu.Item key='/admin/usuarios' icon={<TeamOutlined />}>
            <Link to='/admin/usuarios'>Usuarios</Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default SideMenuComponent;
