import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Layout } from 'antd';
import SideMenuComponent from '../../components/sidemenu/sidemenu.component';
import CommerceComponent from './commerce/commerce.component';
import CategoryComponent from './category/category.component';
import OrderComponent from './orders/orders.component';
import dashboardContainer from './dashboard/dashboard.container';
import HeaderContainer from '../../components/header/header.container';
import menuContainer from './menu/menu.container';
import newproducContainer from './product/newproduc.container';
import productsContainer from './product/products.container';
import userContainer from './users/user.container';
const { Header, Content } = Layout;

class AdminComponent extends Component {
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <SideMenuComponent />
        <Content style={{ padding: 50, background: 'white' }}>
          <Header className='site-layout-background' style={{ padding: 0 }}>
            <HeaderContainer />
          </Header>
          {/* RUTAS */}

          <Route path='/admin/dashboard' component={dashboardContainer} />
          <Route path='/admin/comercios' component={CommerceComponent} />
          <Route path='/admin/categorias' component={CategoryComponent} />
          <Route exact path='/admin/productos' component={productsContainer} />
          <Route exact path='/admin/menuproductos' component={menuContainer} />
          <Route exact path='/admin/pedidos' component={OrderComponent} />
          <Route exact path='/admin/usuarios' component={userContainer} />
          <Route path='/admin/productos/nuevo' component={newproducContainer} />
          <Route
            path='/admin/productos/actualizar'
            component={newproducContainer}
          />

          {/* <Route path='/'>
              <Redirect to='/admin/dashboard' />
            </Route>
            <Route path='*'>
              <Redirect to='/admin/dashboard' />
            </Route> */}
          {/*  */}
        </Content>
      </Layout>
    );
  }
}

export default AdminComponent;
