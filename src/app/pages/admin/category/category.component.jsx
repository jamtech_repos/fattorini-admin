import React, { Component } from 'react';
import { Button, Card, Input, Form, Spin, Popconfirm } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import {
  EditOutlined,
  DeleteOutlined,
  LoadingOutlined,
} from '@ant-design/icons';
import CategoryProvider from '../../../providers/category.provider';
import notifications from '../../../components/notifications/notifications';

import { DndProvider } from 'react-dnd';
import Sortly, {
  ContextProvider,
  useDrag,
  useDrop,
  remove,
} from 'react-sortly';
import { HTML5Backend } from 'react-dnd-html5-backend';

class CategoryComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      loadingData: true,
      loading: false,
      nombre: '',
      categories: [],
      update: false,
      idupdate: '',
    };
  }
  componentDidMount() {
    this.GetCategories();
  }
  GetCategories = () => {
    CategoryProvider.GetCategory().then(
      (res) => {
        var _categories = res.data;
        this.setState({ categories: _categories, loadingData: false });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };

  showForm = () => {
    this.setState({ show: true });
  };
  onCancel = () => {
    this.setState({ show: false });
  };

  onFinish = (value) => {
    console.log(value);
    let data = {
      name: value.name,
      depth: 0,
      id: this.state.categories.length,
    };
    this.setState({ loading: true });
    CategoryProvider.SaveCategory(data).then(
      (res) => {
        this.setState({ loading: false, show: false });
        this.GetCategories();
        notifications.showNotification(
          'success',
          'Categoría creada exitosamente'
        );
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onUpdate = (value) => {
    let data = {
      name: value.name,
    };
    this.setState({ update: false });
    CategoryProvider.UpdateCategory(data, this.state.idupdate).then(
      (res) => {
        this.GetCategories();
        notifications.showNotification(
          'success',
          'Categoría actualizada exitosamente'
        );
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  updateAll(categories) {
    CategoryProvider.UpdateAllCategories({ array: categories }).then(
      (res) => {
        console.log(res);
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  }
  handleChange = (newItems) => {
    this.setState({ categories: newItems }, this.updateAll(newItems));
  };
  onDelete = (id) => {
    let i = this.state.categories.findIndex((item, i) => {
      return item._id === id;
    });
    if (
      !this.state.categories[i + 1] ||
      this.state.categories[i + 1].depth <= this.state.categories[i].depth
    ) {
      let newArray = remove(this.state.categories, i);
      console.log(i, newArray);
      this.setState({ categories: newArray });
      CategoryProvider.DeleteCategory(id).then(
        (res) => {
          this.updateAll(newArray);
        },
        (err) => {
          if (err.data) notifications.showNotification('error', err.data.error);
          else
            notifications.showNotification(
              'error',
              'Error de conexión',
              'Por favor intentelo más tarde'
            );
        }
      );
    } else {
      notifications.showNotification(
        'warning',
        'No podemos eliminar esta categoría',
        'Esta categoría tiene categorias internas, podras eliminarlas si eliminas o modificas las categorias internas.'
      );
    }
  };
  ItemRenderer = (props) => {
    let {
      data: { name, depth, _id },
    } = props;
    const [, drag] = useDrag();
    const [, drop] = useDrop();
    return (
      <div ref={drop}>
        <div
          ref={drag}
          style={{ cursor: 'move', marginBottom: 10, marginLeft: depth * 40 }}
        >
          <Card>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              {this.state.update && this.state.idupdate === _id ? (
                <Form layout='vertical' onFinish={this.onUpdate}>
                  <Form.Item name='name'>
                    <Input
                      value={this.state.name}
                      placeholder='Nombre de la categoría'
                    />
                  </Form.Item>
                </Form>
              ) : (
                <div>{name}</div>
              )}
              <div>
                <EditOutlined
                  className='click'
                  onClick={() =>
                    this.setState({ update: true, name: name, idupdate: _id })
                  }
                />
                <Popconfirm
                  placement='rightBottom'
                  title={
                    '¿Realmente desea eliminar esta categoría?, esto podria afectar a los comercios registrados'
                  }
                  onConfirm={() => this.onDelete(_id)}
                  okText='Si claro'
                  cancelText='No'
                >
                  <DeleteOutlined className='click' />
                </Popconfirm>
              </div>
            </div>
          </Card>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div>
        <div className='row'>
          <div className='col-6'>
            <h5>Categorías</h5>
            <DndProvider backend={HTML5Backend}>
              <ContextProvider>
                <Sortly
                  items={this.state.categories}
                  onChange={this.handleChange}
                >
                  {(props) => this.ItemRenderer({ ...props })}
                </Sortly>
              </ContextProvider>
            </DndProvider>
            {this.state.categories.length <= 0 && !this.state.loadingData ? (
              <h6>No hay cateogorias creadas</h6>
            ) : null}
            {this.state.loadingData ? (
              <Spin
                indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
              />
            ) : null}
          </div>
          <div className='col-6'>
            {this.state.show ? (
              <Card bordered={false}>
                <h5>Nueva categoría</h5>
                <Form layout='vertical' onFinish={this.onFinish}>
                  <Form.Item name='name' label='Nombre de la categoría'>
                    <Input
                      value={this.state.name}
                      placeholder='Nombre de la categoría'
                    />
                  </Form.Item>
                  <Form.Item>
                    <div
                      style={{ display: 'flex', justifyContent: 'flex-end' }}
                    >
                      <Button
                        onClick={this.onCancel}
                        loading={this.state.loading}
                        style={{ margin: '5px' }}
                      >
                        Cancelar
                      </Button>
                      <Button
                        type='primary'
                        htmlType='submit'
                        loading={this.state.loading}
                        style={{ margin: '5px' }}
                      >
                        Guardar
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </Card>
            ) : (
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                <Button type='primary' onClick={this.showForm}>
                  <PlusOutlined /> Agregar categoria
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default CategoryComponent;
