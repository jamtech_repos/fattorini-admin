import React, { Component } from 'react';
import {
  Card,
  Spin,
  Popconfirm,
  Form,
  Input,
  Modal,
  Button,
  Select,
  Upload,
} from 'antd';
import {
  SettingOutlined,
  EditOutlined,
  DeleteOutlined,
  LoadingOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import CommerceProvider from '../../../providers/commerce.provider';
import notifications from '../../../components/notifications/notifications';
import { venezuela } from '../../../providers/venezuela';
import api from '../../../providers/api';

class CommerceComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commerces: [],
      loadingData: true,
      loading: false,
      search: '',
      show: false,
      fileList: [],
      commerceEdit: null,
      cities: venezuela[0].ciudades,
    };
  }
  componentDidMount() {
    this.GetCommerce();
  }

  GetCommerce = () => {
    CommerceProvider.GetCommerces().then(
      (res) => {
        this.setState({ commerces: res.data, loadingData: false });
      },
      (err) => {
        this.setState({ loadingData: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onDelete = (id) => {
    CommerceProvider.DeleteCommerce(id).then(
      (res) => {
        this.GetCommerce();
        notifications.showNotification(
          'success',
          'Comercio eliminado satisfactoriamente'
        );
      },
      (err) => {
        this.setState({ loadingData: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onShow = (data) => {
    this.setState({ show: true, commerceEdit: data });
  };
  onHide = () => {
    this.setState({ show: false, commerceEdit: null });
  };
  onFinish = (value) => {
    if (this.state.fileList.length > 0) {
      value['picture'] = this.state.fileList[0].response.url;
    }
    CommerceProvider.UpdateCommerce(this.state.commerceEdit._id, value).then(
      (res) => {
        this.setState({ show: false, commerceEdit: null });
        this.GetCommerce();
        notifications.showNotification(
          'success',
          'Comercio actualizado satisfactoriamente'
        );
      },
      (err) => {
        this.setState({ loadingData: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  setCities = (estado) => {
    for (let index = 0; index < venezuela.length; index++) {
      const est = venezuela[index];
      if (est.estado === estado) {
        this.setState({ cities: est.ciudades });
        return;
      }
    }
  };
  handleChange = ({ fileList }) => {
    console.log(fileList);
    this.setState({ fileList });
  };
  render() {
    const props = {
      action: api.image + '/file/upload',
      listType: 'picture',
      fileList: this.state.fileList,
      onChange: this.handleChange,
    };
    return (
      <>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <h5>Comercios</h5>
          <Form.Item name='search'>
            <Input
              onChange={(e) => this.setState({ search: e.target.value })}
              value={this.state.search}
              placeholder='Buscar'
            />
          </Form.Item>
        </div>
        {this.state.commerces.length <= 0 && !this.state.loadingData ? (
          <h6>No hay comercios creados</h6>
        ) : null}
        {this.state.loadingData ? (
          <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} />
        ) : null}
        <div className='row'>
          {this.state.commerces
            .filter(
              (a) =>
                this.state.search === '' ||
                a.name.toLowerCase().includes(this.state.search.toLowerCase())
            )
            .map((data) => (
              <div className='col-3' style={{ paddingBottom: 20 }}>
                <Card
                  bordered={false}
                  cover={
                    data.picture || data.picture !== '' ? (
                      <img alt={data.name} src={data.picture} />
                    ) : (
                      <img
                        alt='example'
                        src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
                      />
                    )
                  }
                  actions={[
                    <SettingOutlined className='click' key='setting' />,
                    <EditOutlined
                      onClick={() => this.onShow(data)}
                      className='click'
                      key='edit'
                    />,
                    <Popconfirm
                      placement='top'
                      title={
                        '¿Realmente desea eliminar este comercio?, esto eliminaria todo el contenido creado por los administradores'
                      }
                      onConfirm={() => this.onDelete(data._id)}
                      okText='Si claro'
                      cancelText='No'
                    >
                      <DeleteOutlined className='click' key='ellipsis' />
                    </Popconfirm>,
                  ]}
                >
                  <h6>{data.name}</h6>
                  <p style={{ margin: 0 }}>
                    {data.state}, {data.city}
                  </p>
                  <p style={{ margin: 0 }}>{data.address}</p>
                </Card>
              </div>
            ))}
        </div>
        <Modal
          centered
          closable={false}
          visible={this.state.show}
          footer={[<></>]}
        >
          {this.state.show ? (
            <Form
              layout='vertical'
              initialValues={this.state.commerceEdit}
              onFinish={this.onFinish}
            >
              <Form.Item name='name'>
                <Input
                  value={this.state.commerceEdit.name}
                  placeholder='Nombre del comercio'
                />
              </Form.Item>
              <Form.Item>
                <Upload {...props}>
                  {this.state.fileList.length <= 0 ? (
                    <Button>
                      <UploadOutlined /> Imágen de perfil del comercio
                    </Button>
                  ) : null}
                </Upload>
              </Form.Item>
              <Form.Item
                name='rif'
                rules={[
                  {
                    required: true,
                    message:
                      'Debes indicar el rif o identificación del comercio',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || value.length >= 9) {
                        return Promise.resolve();
                      }
                      return Promise.reject('Debes introducir un rif válido');
                    },
                  }),
                ]}
              >
                <Input
                  size='large'
                  placeholder='Indica el rif de tu comercio'
                />
              </Form.Item>
              <Form.Item name='description'>
                <Input.TextArea
                  size='large'
                  placeholder='Describe tu comercio'
                />
              </Form.Item>
              <Form.Item
                name='state'
                rules={[
                  {
                    required: true,
                    message: 'Debes seleccionar un estado',
                  },
                ]}
              >
                <Select
                  defaultValue={venezuela[0].estado}
                  placeholder='Estado'
                  onChange={this.setCities}
                >
                  {venezuela.map((estado) => (
                    <Select.Option value={estado.estado}>
                      {estado.estado}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name='city'
                rules={[
                  {
                    required: true,
                    message: 'Debes seleccionar una ciudad',
                  },
                ]}
              >
                <Select
                  defaultValue={this.state.cities[0]}
                  placeholder='Ciudad'
                >
                  {this.state.cities.map((city) => (
                    <Select.Option value={city}>{city}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item name='address'>
                <Input size='large' placeholder='Dirección' />
              </Form.Item>
              <Form.Item>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <Button
                    onClick={this.onHide}
                    loading={this.state.loading}
                    style={{ margin: '5px' }}
                  >
                    Cancelar
                  </Button>
                  <Button
                    type='primary'
                    htmlType='submit'
                    loading={this.state.loading}
                    style={{ margin: '5px' }}
                  >
                    Guardar
                  </Button>
                </div>
              </Form.Item>
            </Form>
          ) : null}
        </Modal>
      </>
    );
  }
}

export default CommerceComponent;
