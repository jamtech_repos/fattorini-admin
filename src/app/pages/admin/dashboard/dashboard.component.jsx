import React, { Component } from 'react';
import {
  Card,
  Tag,
  Table,
  Button,
  Form,
  Select,
  Input,
  Upload,
  Modal,
} from 'antd';
import {
  ShoppingCartOutlined,
  EnvironmentOutlined,
  WalletOutlined,
  UserOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import Chart from 'react-apexcharts';
import './dashboard.scss';

import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { venezuela } from '../../../providers/venezuela';
import api from '../../../providers/api';
import notifications from '../../../components/notifications/notifications';
import commerceProvider from '../../../providers/commerce.provider';
class DashboardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileList: [],
      commerceEdit: null,
      show: false,
      cities: venezuela[0].ciudades,
      orders: [
        {
          user: 'user',
          commerce: 'commerce',
          status: 'status',
          date: '01/06/2020 04:35pm',
          price: '12 $',
        },
      ],
      options: {
        chart: {
          width: '100%',
          type: 'line',
          height: 30,
          sparkline: {
            enabled: true,
          },
          zoom: {
            enabled: false,
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          curve: 'smooth',
        },
        xaxis: {
          labels: {
            show: false,
          },
        },
        yaxis: {
          labels: {
            show: false,
          },
        },
        grid: {
          show: false,
          borderColor: '#fff',
          padding: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
          },
        },
      },
      series: [
        {
          name: 'series-1',
          data: [10, 40, 100, 50, 30, 150, 50, 200],
        },
      ],
    };
  }
  onShow = (data) => {
    this.setState({ show: true, commerceEdit: data });
  };
  onHide = () => {
    this.setState({ show: false, commerceEdit: null });
  };
  onFinish = (value) => {
    if (this.state.fileList.length > 0) {
      value['picture'] = this.state.fileList[0].response.url;
    }
    commerceProvider.UpdateCommerce(this.state.commerceEdit._id, value).then(
      (res) => {
        this.setState({ show: false, commerceEdit: null });
        notifications.showNotification(
          'success',
          'Comercio actualizado satisfactoriamente'
        );
        window.location.reload();
      },
      (err) => {
        this.setState({ loadingData: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  setCities = (estado) => {
    for (let index = 0; index < venezuela.length; index++) {
      const est = venezuela[index];
      if (est.estado === estado) {
        this.setState({ cities: est.ciudades });
        return;
      }
    }
  };
  handleChange = ({ fileList }) => {
    console.log(fileList);
    this.setState({ fileList });
  };
  render() {
    const props = {
      action: api.image + '/file/upload',
      listType: 'picture',
      fileList: this.state.fileList,
      onChange: this.handleChange,
    };
    const columns = [
      { title: 'Usuario', dataIndex: 'user', key: 'user' },
      { title: 'Comercio', dataIndex: 'commerce', key: 'commerce' },
      { title: 'Fecha', dataIndex: 'date', key: 'date' },
      {
        title: 'Estatus',
        dataIndex: 'status',
        key: 'status',
        render: () => (
          <span>
            <Tag color='green'>Entregado</Tag>
          </span>
        ),
      },
      {
        title: '',
        key: '',
        render: () => (
          <div>
            <EditOutlined className='click' />
            <DeleteOutlined className='click' />
          </div>
        ),
      },
    ];
    return (
      <div>
        <div className='row'>
          <div className={this.props.role === 'CA' ? 'col-lg-9' : 'col-lg-6'}>
            <div className='row' style={{ height: '100%' }}>
              {this.props.role === 'CA' ? (
                <div className='col-lg-4' style={{ margin: '15px 0' }}>
                  <Card
                    bordered={false}
                    cover={
                      this.props.commerce.picture ||
                      this.props.commerce.picture !== '' ? (
                        <img
                          alt={this.props.commerce.name}
                          src={this.props.commerce.picture}
                        />
                      ) : (
                        <img
                          alt='example'
                          src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
                        />
                      )
                    }
                    actions={[
                      <EditOutlined
                        onClick={() => this.onShow(this.props.commerce)}
                        className='click'
                        key='edit'
                      />,
                    ]}
                  >
                    <h6>{this.props.commerce.name}</h6>
                    <p style={{ margin: 0 }}>
                      {this.props.commerce.state}, {this.props.commerce.city}
                    </p>
                    <p style={{ margin: 0 }}>{this.props.commerce.address}</p>
                  </Card>
                </div>
              ) : null}
              <div
                className={this.props.role === 'CA' ? 'col-lg-4' : 'col-lg-6'}
                style={{ margin: '15px 0' }}
              >
                <Card className='welcome' bordered={false}>
                  <h3>Hola {this.props.user.firstname}!</h3>
                  <h6>Bienvenido de nuevo.</h6>
                </Card>
              </div>
              <div
                className={this.props.role === 'CA' ? 'col-lg-4' : 'col-lg-6'}
                style={{ margin: '15px 0' }}
              >
                <Card className='currency' bordered={false}>
                  <span className='currency-percent up'>up 0.05%</span>
                  <EditOutlined className='currency-edit click' />
                  <h3>190.000 Bs</h3>
                  <h6>Tasa del dia</h6>
                </Card>
              </div>
            </div>
          </div>
          <div className={this.props.role === 'CA' ? 'col-lg-3' : 'col-lg-6'}>
            <div className='row'>
              <div
                className={this.props.role === 'CA' ? 'col-lg-12' : 'col-lg-6'}
                style={{ margin: '15px 0' }}
              >
                <Card className='info-card' bordered={false}>
                  <div className='row'>
                    <div
                      className='col-3'
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <ShoppingCartOutlined />
                    </div>
                    <div className='col-9' style={{ textAlign: 'end' }}>
                      <h2>60/200</h2>
                      <span className='subtitle'>Pedidos Recibidos</span>
                    </div>
                  </div>
                  <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type='line'
                    height='60'
                    width='250'
                  />
                </Card>
              </div>
              <div
                className={this.props.role === 'CA' ? 'col-lg-12' : 'col-lg-6'}
                style={{ margin: '15px 0' }}
              >
                <Card className='info-card' bordered={false}>
                  <div
                    className='row'
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <div className='col-3'>
                      <EnvironmentOutlined />
                    </div>
                    <div className='col-9' style={{ textAlign: 'end' }}>
                      <h2>20/60</h2>
                      <span className='subtitle'>Pedidos Entregados</span>
                    </div>
                  </div>
                  <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type='line'
                    height='60'
                    width='250'
                  />
                </Card>
              </div>
              <div
                className={this.props.role === 'CA' ? 'col-lg-12' : 'col-lg-6'}
                style={{ margin: '15px 0' }}
              >
                <Card className='info-card' bordered={false}>
                  <div
                    className='row'
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <div className='col-3'>
                      <WalletOutlined />
                    </div>
                    <div className='col-9' style={{ textAlign: 'end' }}>
                      <h2>1200 $</h2>
                      <span className='subtitle'>Saldo disponible</span>
                    </div>
                  </div>
                </Card>
              </div>
              {this.props.role === 'A' ? (
                <div className='col-6' style={{ margin: '15px 0' }}>
                  <Card className='info-card' bordered={false}>
                    <div
                      className='row'
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <div className='col-3'>
                        <UserOutlined />
                      </div>
                      <div className='col-9' style={{ textAlign: 'end' }}>
                        <h2>50</h2>
                        <span className='subtitle'>Nuevos usuarios</span>
                      </div>
                    </div>
                  </Card>
                </div>
              ) : null}
            </div>
          </div>
        </div>

        <div className='row' style={{ marginTop: 30 }}>
          <div className='col-sm-6'>
            <Card bordered={false}>
              <h5>Pedidos recientes</h5>
              <Table columns={columns} dataSource={this.state.orders} />
            </Card>
          </div>
        </div>
        <Modal
          centered
          closable={false}
          visible={this.state.show}
          footer={[<></>]}
        >
          {this.state.show ? (
            <Form
              layout='vertical'
              initialValues={this.state.commerceEdit}
              onFinish={this.onFinish}
            >
              <Form.Item name='name'>
                <Input
                  value={this.state.commerceEdit.name}
                  placeholder='Nombre del comercio'
                />
              </Form.Item>
              <Form.Item>
                <Upload {...props}>
                  {this.state.fileList.length <= 0 ? (
                    <Button>
                      <UploadOutlined /> Imágen de perfil del comercio
                    </Button>
                  ) : null}
                </Upload>
              </Form.Item>
              <Form.Item
                name='rif'
                rules={[
                  {
                    required: true,
                    message:
                      'Debes indicar el rif o identificación del comercio',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || value.length >= 9) {
                        return Promise.resolve();
                      }
                      return Promise.reject('Debes introducir un rif válido');
                    },
                  }),
                ]}
              >
                <Input
                  size='large'
                  placeholder='Indica el rif de tu comercio'
                />
              </Form.Item>
              <Form.Item name='description'>
                <Input.TextArea
                  size='large'
                  placeholder='Describe tu comercio'
                />
              </Form.Item>
              <Form.Item
                name='state'
                rules={[
                  {
                    required: true,
                    message: 'Debes seleccionar un estado',
                  },
                ]}
              >
                <Select
                  defaultValue={venezuela[0].estado}
                  placeholder='Estado'
                  onChange={this.setCities}
                >
                  {venezuela.map((estado) => (
                    <Select.Option value={estado.estado}>
                      {estado.estado}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name='city'
                rules={[
                  {
                    required: true,
                    message: 'Debes seleccionar una ciudad',
                  },
                ]}
              >
                <Select
                  defaultValue={this.state.cities[0]}
                  placeholder='Ciudad'
                >
                  {this.state.cities.map((city) => (
                    <Select.Option value={city}>{city}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item name='address'>
                <Input size='large' placeholder='Dirección' />
              </Form.Item>
              <Form.Item>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <Button
                    onClick={this.onHide}
                    loading={this.state.loading}
                    style={{ margin: '5px' }}
                  >
                    Cancelar
                  </Button>
                  <Button
                    type='primary'
                    htmlType='submit'
                    loading={this.state.loading}
                    style={{ margin: '5px' }}
                  >
                    Guardar
                  </Button>
                </div>
              </Form.Item>
            </Form>
          ) : null}
        </Modal>
      </div>
    );
  }
}

export default DashboardComponent;
