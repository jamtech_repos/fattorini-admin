import {
  connect
} from 'react-redux';
import DashboardComponent from './dashboard.component';

const mapStateToProps = state => ({
  user: state.auth.user,
  role: state.auth.role,
  commerce: state.auth.commerce
});

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardComponent);