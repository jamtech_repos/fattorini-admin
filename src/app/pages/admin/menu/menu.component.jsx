import React, { Component } from 'react';
import { Button, Card, Input, Form, Spin, Popconfirm } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import {
  EditOutlined,
  DeleteOutlined,
  LoadingOutlined,
  CloseOutlined,
  CheckOutlined,
} from '@ant-design/icons';
import MenuProvider from '../../../providers/menu.provider';
import notifications from '../../../components/notifications/notifications';

import { DndProvider } from 'react-dnd';
import Sortly, {
  ContextProvider,
  useDrag,
  useDrop,
  remove,
} from 'react-sortly';
import { HTML5Backend } from 'react-dnd-html5-backend';

class MenuComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      loadingData: true,
      loading: false,
      nombre: '',
      categories: [],
      update: false,
      idupdate: '',
    };
  }
  componentDidMount() {
    this.GetMenu();
  }
  GetMenu = () => {
    console.log(this.props.commerce._id);
    MenuProvider.GetMenu(this.props.commerce._id).then(
      (res) => {
        var _categories = res.data;
        this.setState({ categories: _categories, loadingData: false });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };

  showForm = () => {
    this.setState({ show: true });
  };
  onCancel = () => {
    this.setState({ show: false });
  };

  onFinish = (value) => {
    console.log(value);
    let data = {
      commerce: this.props.commerce._id,
      name: value.name,
      depth: 0,
      id: this.state.categories.length,
    };
    this.setState({ loading: true });
    MenuProvider.SaveMenu(data).then(
      (res) => {
        this.setState({ loading: false, show: false });
        this.GetMenu();
        notifications.showNotification('success', 'Menú creado exitosamente');
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onUpdate = (value) => {
    let data = {
      name: value.name,
    };
    this.setState({ update: false });
    MenuProvider.UpdateMenu(data, this.state.idupdate).then(
      (res) => {
        this.GetMenu();
        notifications.showNotification(
          'success',
          'Menú actualizado exitosamente'
        );
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  updateAll(categories) {
    MenuProvider.UpdateAllMenu({ array: categories }).then(
      (res) => {
        console.log(res);
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  }
  handleChange = (newItems) => {
    this.setState({ categories: newItems }, this.updateAll(newItems));
  };
  onDelete = (id) => {
    let i = this.state.categories.findIndex((item, i) => {
      return item._id === id;
    });
    if (
      !this.state.categories[i + 1] ||
      this.state.categories[i + 1].depth <= this.state.categories[i].depth
    ) {
      let newArray = remove(this.state.categories, i);
      console.log(i, newArray);
      this.setState({ categories: newArray });
      MenuProvider.DeleteMenu(id).then(
        (res) => {
          this.updateAll(newArray);
          notifications.showNotification(
            'success',
            'Menú eliminado exitosamente'
          );
        },
        (err) => {
          if (err.data) notifications.showNotification('error', err.data.error);
          else
            notifications.showNotification(
              'error',
              'Error de conexión',
              'Por favor intentelo más tarde'
            );
        }
      );
    } else {
      notifications.showNotification(
        'warning',
        'No podemos eliminar esta categoría',
        'Esta categoría tiene categorias internas, podras eliminarlas si eliminas o modificas las categorias internas.'
      );
    }
  };
  ItemRenderer = (props) => {
    let {
      data: { name, depth, _id },
    } = props;
    const [, drag] = useDrag();
    const [, drop] = useDrop();
    const styles = {
      marginLeft: '5px',
      padding: '4px 15px',
    };
    return (
      <div ref={drop}>
        <div
          ref={drag}
          style={{ cursor: 'move', marginBottom: 10, marginLeft: depth * 40 }}
        >
          <Card>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              {this.state.update && this.state.idupdate === _id ? (
                <Form
                  layout='inline'
                  initialValues={{ name: this.state.name }}
                  onFinish={this.onUpdate}
                >
                  <Form.Item
                    name='name'
                    rules={[
                      {
                        required: true,
                        message: 'Este campo no puede estar vacio',
                      },
                    ]}
                  >
                    <Input
                      value={this.state.name}
                      placeholder='Nombre del menú'
                    />
                  </Form.Item>
                  <Form.Item>
                    <div style={{ display: 'flex' }}>
                      <Button
                        onClick={() => {
                          this.setState({ update: false });
                        }}
                        loading={this.state.loading}
                        style={styles}
                      >
                        <CloseOutlined />
                      </Button>
                      <Button
                        onClick={this.onCancel}
                        loading={this.state.loading}
                        htmlType='submit'
                        style={styles}
                      >
                        <CheckOutlined />
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              ) : (
                <div>{name}</div>
              )}
              <div>
                {!(this.state.update && this.state.idupdate === _id) ? (
                  <EditOutlined
                    className='click'
                    onClick={() =>
                      this.setState({ update: true, name: name, idupdate: _id })
                    }
                  />
                ) : null}
                <Popconfirm
                  placement='rightBottom'
                  title={
                    '¿Realmente desea eliminar esta categoría?, esto podria afectar a los comercios registrados'
                  }
                  onConfirm={() => this.onDelete(_id)}
                  okText='Si claro'
                  cancelText='No'
                >
                  <DeleteOutlined className='click' />
                </Popconfirm>
              </div>
            </div>
          </Card>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div>
        <div className='row'>
          <div className='col-6'>
            <h5>Menú</h5>
            <DndProvider backend={HTML5Backend}>
              <ContextProvider>
                <Sortly
                  items={this.state.categories}
                  onChange={this.handleChange}
                >
                  {(props) => this.ItemRenderer({ ...props })}
                </Sortly>
              </ContextProvider>
            </DndProvider>
            {this.state.categories.length <= 0 && !this.state.loadingData ? (
              <h6>No hay menu creados</h6>
            ) : null}
            {this.state.loadingData ? (
              <Spin
                indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
              />
            ) : null}
          </div>
          <div className='col-6'>
            {this.state.show ? (
              <Card bordered={false}>
                <h5>Nuevo menú</h5>
                <Form layout='vertical' onFinish={this.onFinish}>
                  <Form.Item
                    name='name'
                    label='Nombre del menú'
                    rules={[
                      {
                        required: true,
                        message: 'Este campo no puede estar vacio',
                      },
                    ]}
                  >
                    <Input
                      value={this.state.name}
                      placeholder='Nombre del menú'
                    />
                  </Form.Item>
                  <Form.Item>
                    <div
                      style={{ display: 'flex', justifyContent: 'flex-end' }}
                    >
                      <Button
                        onClick={this.onCancel}
                        loading={this.state.loading}
                        style={{ margin: '5px' }}
                      >
                        Cancelar
                      </Button>
                      <Button
                        type='primary'
                        htmlType='submit'
                        loading={this.state.loading}
                        style={{ margin: '5px' }}
                      >
                        Guardar
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </Card>
            ) : (
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                <Button type='primary' onClick={this.showForm}>
                  <PlusOutlined /> Agregar menú
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default MenuComponent;
