import {
  connect
} from 'react-redux';
import MenuComponent from './menu.component';

const mapStateToProps = state => ({
  commerce: state.auth.commerce
});

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuComponent);