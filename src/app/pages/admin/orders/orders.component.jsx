import React, { Component } from 'react';
import { Table, Tag, Card } from 'antd';

import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
/* import notifications from '../../../components/notifications/notifications'; */

class OrderComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [
        {
          user: 'user',
          commerce: 'commerce',
          status: 'status',
          date: '01/06/2020 04:35pm',
          price: '12 $',
        },
      ],
    };
  }
  componentWillMount() {
    /* ProductProvider.GetTypeProducts(this.props.type).then(
      (res) => {
        this.setState({ monturas: res.data });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    ); */
  }
  render() {
    const columns = [
      { title: 'Usuario', dataIndex: 'user', key: 'user' },
      { title: 'Comercio', dataIndex: 'commerce', key: 'commerce' },
      { title: 'Fecha', dataIndex: 'date', key: 'date' },
      { title: 'Monto', dataIndex: 'price', key: 'price' },
      {
        title: 'Estatus',
        dataIndex: 'status',
        key: 'status',
        render: () => (
          <span>
            <Tag color='green'>Entregado</Tag>
          </span>
        ),
      },
      {
        title: '',
        key: '',
        render: () => (
          <div>
            <EditOutlined className='click' />
            <DeleteOutlined className='click' />
          </div>
        ),
      },
    ];
    return (
      <div>
        <Card bordered={false}>
          <Table
            columns={columns}
            expandable={{ expandedRowRender }}
            dataSource={this.state.orders}
          />
        </Card>
      </div>
    );
  }
}
const expandedRowRender = () => {
  const columns = [
    { dataIndex: 'product', key: 'product' },
    { dataIndex: 'description', key: 'description' },
    {
      dataIndex: 'price',
      key: 'price',
    },
  ];

  const data = [];
  for (let i = 0; i < 3; ++i) {
    data.push({
      key: i,
      product: 'Producto ' + (i + 1),
      description: 'producto maqueta',
      price: '12 $',
    });
  }
  return (
    <Table
      showHeader={false}
      columns={columns}
      dataSource={data}
      pagination={false}
    />
  );
};
export default OrderComponent;
