import React, { Component } from 'react';
import {
  Input,
  Form,
  Select,
  Button,
  Card,
  Upload,
  InputNumber,
  Switch,
} from 'antd';

import { UploadOutlined, CloseOutlined } from '@ant-design/icons';
import ProductProvider from '../../../providers/product.provider';
import Api from '../../../providers/api';
import notifications from '../../../components/notifications/notifications';
import menuProvider from '../../../providers/menu.provider';

class NewProductComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      fileList: [],
      menu: [],
      update: { _id: '', currency: 'USD' },
    };
  }
  componentDidMount() {
    this.GetMenu();
    console.log(this.props.location.state);
    if (this.props.location.state) {
      this.setState({ update: this.props.location.state.update });
    }
  }
  GetMenu = () => {
    menuProvider.GetMenu(this.props.commerce._id).then(
      (res) => {
        var menu = res.data;
        this.setState({ menu: menu, loadingData: false });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onCancel = () => {
    this.setState({
      loading: false,
    });
    this.props.history.push('/admin/productos');
  };
  saveProduct = (values) => {
    if (this.state.fileList.length <= 0) {
      this.setState({ loading: false });
      notifications.showNotification(
        'error',
        'Debe agregar imagenes al producto'
      );
    } else {
      console.log('Received values of form: ', values);
      var data = {
        picture: [],
        name: values.name,
        description: values.description,
        price: values.price,
        currency: values.currency,
        offer: values.offer,
        offerpercent: values.offerpercent,
        commerce: this.props.commerce._id,
        menu: values.menu,
      };
      for (let i = 0; i < this.state.fileList.length; i++) {
        data.picture.push(this.state.fileList[i].response.url);
      }
      ProductProvider.SaveProduct(data).then(
        (res) => {
          console.log(res.data);
          this.setState({ loading: false });
          notifications.showNotification(
            'success',
            'Producto creado exitosamente'
          );
          this.props.history.push('/admin/productos');
        },
        (err) => {
          this.setState({ loading: false });
          console.log(err);
          notifications.showNotification(
            'error',
            'Error al crear producto',
            err.data.error
          );
        }
      );
    }
  };
  updateProduct = (values) => {
    if (
      this.state.fileList.length <= 0 &&
      this.state.update.picture.length <= 0
    ) {
      this.setState({ loading: false });
      notifications.showNotification(
        'error',
        'Debe agregar imagenes al producto'
      );
    } else {
      console.log('Received values of form: ', values);
      var data = {
        picture: this.state.update.picture,
        name: values.name,
        description: values.description,
        price: values.price,
        currency: values.currency,
        offer: values.offer,
        offerpercent: values.offerpercent,
        commerce: this.props.commerce._id,
        menu: values.menu,
      };
      for (let i = 0; i < this.state.fileList.length; i++) {
        data.picture.push(this.state.fileList[i].response.url);
      }
      ProductProvider.UpdateProduct(this.state.update._id, data).then(
        (res) => {
          console.log(res.data);
          this.setState({ loading: false });
          notifications.showNotification(
            'success',
            'Producto creado exitosamente'
          );
          this.props.history.push('/admin/productos');
        },
        (err) => {
          this.setState({ loading: false });
          console.log(err);
          notifications.showNotification(
            'error',
            'Error al crear producto',
            err.data.error
          );
        }
      );
    }
  };
  onFinish = (values) => {
    this.setState({ loading: true });
    if (this.state.update._id === '') {
      this.saveProduct(values);
    } else {
      this.updateProduct(values);
    }
  };
  handleChange = ({ fileList }) => {
    console.log(fileList);
    this.setState({ fileList });
  };
  deleteImageUpdate = (index) => {
    let update = this.state.update;
    update.picture.splice(index, 1);
    this.setState({ update: update });
  };

  render() {
    const props = {
      action: Api.image + '/file/upload',
      listType: 'picture',
      fileList: this.state.fileList,
      onChange: this.handleChange,
    };
    let { update } = this.state;
    if (this.props.location.state) {
      update = this.props.location.state.update;
    }
    return (
      <>
        {this.state.update._id === '' ? (
          <h5>Crear nuevo producto</h5>
        ) : (
          <h5>Actualizar producto</h5>
        )}
        <div className='row'>
          <div className='col-lg-6'>
            <Card bordered={false}>
              <h6>Imagenes del producto</h6>
              <Upload {...props}>
                <Button>
                  <UploadOutlined /> Subir imagen
                </Button>
              </Upload>
              {this.state.update._id !== '' ? (
                <div style={{ display: 'flex', marginTop: 20 }}>
                  {this.state.update.picture.map((image, index) => (
                    <div
                      key={index}
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                      }}
                    >
                      <img
                        style={{ width: 90, height: 90, margin: 10 }}
                        src={image}
                        alt='imágen del producto'
                      />
                      <CloseOutlined
                        onClick={() => this.deleteImageUpdate(index)}
                      />
                    </div>
                  ))}
                </div>
              ) : null}
            </Card>
          </div>
          <div className='col-lg-6'>
            <Card bordered={false}>
              <h6>Detalle del producto</h6>
              <Form
                layout='vertical'
                initialValues={update}
                onFinish={this.onFinish}
              >
                <Form.Item
                  name='name'
                  rules={[
                    {
                      required: true,
                      message: 'Debe indicar el nombre del producto',
                    },
                  ]}
                >
                  <Input placeholder='Nombre del producto' />
                </Form.Item>
                <Form.Item
                  name='price'
                  rules={[
                    { required: true, message: 'El precio es requerido' },
                  ]}
                >
                  <InputNumber
                    defaultValue={1}
                    min={1}
                    placeholder='Precio del producto'
                  />
                </Form.Item>
                <Form.Item
                  name='currency'
                  rules={[
                    {
                      required: true,
                      message: 'El tipo de moneda es requerido',
                    },
                  ]}
                >
                  <Select defaultValue='USD' placeholder='Moneda'>
                    <Select.Option value='USD'>USD</Select.Option>
                    <Select.Option value='BS'>Bs</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item name='offer' label='oferta' valuePropName='checked'>
                  <Switch />
                </Form.Item>
                <Form.Item name='offerpercent'>
                  <InputNumber
                    defaultValue={0}
                    min={0}
                    max={100}
                    placeholder='Porcentaje de oferta'
                  />
                </Form.Item>
                <Form.Item name='description'>
                  <Input.TextArea placeholder='Descripción' />
                </Form.Item>
                <Form.Item
                  name='menu'
                  rules={[
                    {
                      required: true,
                      message: 'Debe seleccionar un item del menú',
                    },
                  ]}
                >
                  <Select placeholder='Menú'>
                    {this.state.menu.map((data, index) => (
                      <Select.Option
                        style={{ marginLeft: data.depth * 20 }}
                        value={data._id}
                        key={index}
                      >
                        {data.name}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item>
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button
                      onClick={this.onCancel}
                      loading={this.state.loading}
                      style={{ margin: '5px' }}
                    >
                      Cancelar
                    </Button>
                    <Button
                      type='primary'
                      htmlType='submit'
                      loading={this.state.loading}
                      style={{ margin: '5px' }}
                    >
                      Guardar
                    </Button>
                  </div>
                </Form.Item>
              </Form>
            </Card>
          </div>
        </div>
      </>
    );
  }
}

export default NewProductComponent;
