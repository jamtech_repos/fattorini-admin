import React, { Component } from 'react';
import { Button, Card, Spin, Popconfirm } from 'antd';
import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  LoadingOutlined,
} from '@ant-design/icons';
import notifications from '../../../components/notifications/notifications';
import productProvider from '../../../providers/product.provider';
import menuProvider from '../../../providers/menu.provider';

class ProductsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      products: [],
      menu: [],
    };
  }
  componentDidMount() {
    this.GetProducts();
    this.GetMenu();
  }
  GetMenu = () => {
    menuProvider.GetMenu(this.props.commerce._id).then(
      (res) => {
        var menu = res.data;
        this.setState({ menu: menu, loadingData: false });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  newProduct = () => {
    this.props.history.push('/admin/productos/nuevo');
  };

  GetProducts = () => {
    productProvider.GetByCommerce(this.props.commerce._id).then(
      (res) => {
        var producto = res.data;
        this.setState({ products: producto, loading: false });
      },
      (err) => {
        this.setState({ loading: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onDelete = (id) => {
    productProvider.DeleteProduct(id).then(
      (res) => {
        notifications.showNotification(
          'success',
          'Producto eliminado exitosamente'
        );
        this.GetProducts();
        this.setState({ loading: false });
      },
      (err) => {
        this.setState({ loading: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  getOffer = (percent, price) => {
    let result = price * (percent / 100);
    return price - result;
  };
  getMenuName = (id) => {
    for (let i = 0; i < this.state.menu.length; i++) {
      const element = this.state.menu[i];
      if (element._id === id) {
        return element.name;
      }
    }
  };
  openUpdate = (item) => {
    this.props.history.push({
      pathname: '/admin/productos/actualizar',
      state: { update: item },
    });
  };
  render() {
    return (
      <div>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: ' space-between',
          }}
        >
          <h5>Productos</h5>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button type='primary' onClick={this.newProduct}>
              <PlusOutlined /> Agregar Producto
            </Button>
          </div>
        </div>

        {this.state.products.length <= 0 && !this.state.loading ? (
          <h6>No hay productos creados</h6>
        ) : null}
        {this.state.loading ? (
          <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} />
        ) : null}
        <div className='row'>
          {this.state.products.map((data) => (
            <div className='col-3'>
              <Card
                bordered={false}
                cover={<img alt='example' src={data.picture[0]} />}
                actions={[
                  <EditOutlined
                    onClick={() => this.openUpdate(data)}
                    className='click'
                    key='edit'
                  />,
                  <Popconfirm
                    placement='rightBottom'
                    title={'¿Realmente desea eliminar este producto?'}
                    onConfirm={() => this.onDelete(data._id)}
                    okText='Si claro'
                    cancelText='No'
                  >
                    <DeleteOutlined className='click' key='ellipsis' />
                  </Popconfirm>,
                ]}
              >
                <p
                  style={{
                    margin: 0,
                    fontSize: 14,
                    textTransform: 'uppercase',
                    fontWeight: 'lighter',
                  }}
                >
                  {this.getMenuName(data.menu)}
                </p>
                <h5>{data.name}</h5>

                {!data.offer ? (
                  <p style={{ fontSize: '20px', margin: 0 }}>${data.price}</p>
                ) : (
                  <p style={{ fontSize: '20px', margin: 0 }}>
                    <span
                      style={{
                        fontSize: '16px',
                        textDecoration: 'line-through',
                      }}
                    >
                      ${data.price}
                    </span>{' '}
                    ${this.getOffer(data.offerpercent, data.price)}
                  </p>
                )}
              </Card>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default ProductsComponent;
