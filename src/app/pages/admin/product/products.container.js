import {
  connect
} from 'react-redux';
import ProductsComponent from './products.component';

const mapStateToProps = state => ({
  commerce: state.auth.commerce
});

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsComponent);