import React, { Component } from 'react';
import {
  Table,
  Tag,
  Tabs,
  Button,
  Avatar,
  Modal,
  Input,
  Form,
  Popconfirm,
  Select,
} from 'antd';

import {
  PlusOutlined,
  CloseOutlined,
  UserOutlined,
  EditOutlined,
  CheckOutlined,
} from '@ant-design/icons';
import commerceProvider from '../../../providers/commerce.provider';
import notifications from '../../../components/notifications/notifications';
import userProvider from '../../../providers/user.provider';
/* import notifications from '../../../components/notifications/notifications'; */
const { TabPane } = Tabs;
class UserComponent extends Component {
  formRef = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      update: null,
      users: [],
      role: 'A',
      visible: false,
    };
  }
  componentDidMount() {
    if (this.props.role === 'CA') {
      this.ListAdmins();
    } else {
      this.ListUsersByRole(this.state.role);
    }
    /* ProductProvider.GetTypeProducts(this.props.type).then(
      (res) => {
        this.setState({ monturas: res.data });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    ); */
  }
  ListAdmins = () => {
    commerceProvider.ListAdmins(this.props.commerce._id).then(
      (res) => {
        this.setState({ users: res.data.user });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  ListUsersByRole = (role) => {
    userProvider.ListUserByRole(role).then(
      (res) => {
        this.setState({ users: res.data });
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  saveData = (value) => {
    this.setState({ loading: true });
    if (this.state.update) {
      this.updateUser(this.state.update._id, value);
    } else {
      if (this.props.role === 'CA') {
        this.createUserCA(value, this.props.commerce._id);
      } else {
        this.SaveUser(value);
      }
    }
  };
  createUserCA = (values, commerce) => {
    userProvider.RegisterCA(values, commerce).then(
      (userres) => {
        this.ListAdmins();
        notifications.showNotification(
          'success',
          'Usuario creado exitosamente'
        );
        this.closeModal();
      },
      (err) => {
        this.setState({
          loading: false,
        });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  SaveUser = (values) => {
    userProvider.SaveUser(values).then(
      (userres) => {
        notifications.showNotification(
          'success',
          'Usuario creado exitosamente'
        );
        this.closeModal();
        this.ListUsersByRole(this.state.role);
      },
      (err) => {
        this.setState({
          loading: false,
        });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  updateUser = (id, data) => {
    if (data.password === '') delete data.password;
    userProvider.UpdateUser(id, data).then(
      (res) => {
        if (this.props.role === 'CA') {
          this.ListAdmins();
        } else {
        }
        notifications.showNotification(
          'success',
          'Usuario actualizado exitosamente'
        );
        this.closeModal();
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  changeUserEstatus = (id, status) => {
    userProvider.UpdateUser(id, { status: status }).then(
      (res) => {
        if (this.props.role === 'CA') {
          this.ListAdmins();
        } else {
          this.ListUsersByRole(this.state.role);
        }
        notifications.showNotification(
          'success',
          'Usuario actualizado exitosamente'
        );
      },
      (err) => {
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  openModal = () => {
    this.setState({ visible: true });
    setTimeout(() => this.formRef.current.resetFields(), 500);
  };
  openEditModal = (item) => {
    item.password = '';
    setTimeout(() => this.formRef.current.resetFields(), 500);
    this.setState({ visible: true, update: item });
  };
  closeModal = () => {
    this.setState({ visible: false, update: null, loading: false });
    setTimeout(() => this.formRef.current.resetFields(), 500);
  };
  chageTabs = (e) => {
    console.log(e);
    switch (e) {
      case '1': {
        this.setState({ role: 'A', users: [] });
        this.ListUsersByRole('A');
        break;
      }
      case '2': {
        this.setState({ role: 'C', users: [] });
        this.ListUsersByRole('C');
        break;
      }
      default:
        break;
    }
  };
  onDelete = (id) => {};
  render() {
    const columns = [
      { key: 'profile_pic', render: () => <Avatar icon={<UserOutlined />} /> },
      { title: 'Nombre', dataIndex: 'firstname', key: 'firstname' },
      { title: 'Apellido', dataIndex: 'lastname', key: 'lastname' },
      { title: 'Correo', dataIndex: 'email', key: 'email' },
      {
        title: 'Estatus',
        dataIndex: 'status',
        key: 'status',
        render: (value) => (
          <span>
            {value ? (
              <Tag color='green'>Activo</Tag>
            ) : (
              <Tag color='red'>Inactivo</Tag>
            )}
          </span>
        ),
      },
      {
        title: '',
        key: '',
        render: (data) => (
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <EditOutlined onClick={() => this.openEditModal(data)} />
            {data.status ? (
              <Popconfirm
                placement='rightBottom'
                title={'¿Realmente desea eliminar este producto?'}
                onConfirm={() => this.changeUserEstatus(data._id, false)}
                okText='Si claro'
                cancelText='No'
              >
                <CloseOutlined className='click' />
              </Popconfirm>
            ) : (
              <CheckOutlined
                className='click'
                onClick={() => this.changeUserEstatus(data._id, true)}
              />
            )}
          </div>
        ),
      },
    ];
    return (
      <>
        {this.props.role === 'A' ? (
          <div>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button type='primary' onClick={this.openModal}>
                <PlusOutlined onClick={this.openModal} /> Agregar usuario
              </Button>
            </div>
            <Tabs
              defaultActiveKey='1'
              size={'large'}
              style={{ marginBottom: 32 }}
              onChange={this.chageTabs}
            >
              <TabPane tab='Administradores' key='1'>
                <Table
                  showHeader={false}
                  columns={columns}
                  dataSource={this.state.users}
                  pagination={false}
                />
              </TabPane>
              <TabPane tab='Conductores' key='2'>
                <Table
                  showHeader={false}
                  columns={columns}
                  dataSource={this.state.users}
                  pagination={false}
                />
              </TabPane>
              <TabPane tab='Clientes' key='3'>
                <Table
                  showHeader={false}
                  columns={columns}
                  dataSource={this.state.users}
                  pagination={false}
                />
              </TabPane>
            </Tabs>
          </div>
        ) : (
          <div>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button type='primary' onClick={this.openModal}>
                <PlusOutlined /> Agregar Administrador
              </Button>
            </div>
            <Tabs
              defaultActiveKey='1'
              size={'large'}
              style={{ marginBottom: 32 }}
            >
              <TabPane tab='Administradores' key='1'>
                <Table
                  showHeader={false}
                  columns={columns}
                  dataSource={this.state.users}
                  pagination={false}
                />
              </TabPane>
            </Tabs>
          </div>
        )}
        <Modal
          title='Registro de usuario'
          visible={this.state.visible}
          onCancel={this.closeModal}
          footer={[<></>]}
        >
          <Form
            layout='vertical'
            onFinish={this.saveData}
            initialValues={this.state.update}
            ref={this.formRef}
          >
            <Form.Item
              name='firstname'
              rules={[{ required: true, message: 'Este campo es obligatorio' }]}
            >
              <Input placeholder='Nombre' />
            </Form.Item>
            <Form.Item
              name='lastname'
              rules={[{ required: true, message: 'Este campo es obligatorio' }]}
            >
              <Input placeholder='Apellido' />
            </Form.Item>
            <Form.Item
              name='email'
              rules={[
                {
                  type: 'email',
                  message: 'Debes introducir un email valido',
                },
                {
                  required: true,
                  message: 'Debes indicar tu correo de usuario',
                },
              ]}
            >
              <Input size='large' placeholder='Correo' />
            </Form.Item>
            <Form.Item
              name='password'
              rules={[
                {
                  required: this.state.update ? false : true,
                  message: 'Debes registrar una contraseña',
                },
              ]}
            >
              <Input.Password
                size='large'
                type='password'
                placeholder='Contraseña'
              />
            </Form.Item>
            <Form.Item
              name='passwordconfirm'
              rules={[
                {
                  required: this.state.update ? false : true,
                  message: 'Debes confirmar tu contraseña',
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject('Las contraseñas no coinciden');
                  },
                }),
              ]}
            >
              <Input
                size='large'
                type='password'
                placeholder='Confirmar contraseña'
              />
            </Form.Item>
            {this.props.role === 'A' ? (
              <Form.Item
                name='role'
                rules={[
                  {
                    required: this.state.role !== 'A' ? false : true,
                    message: 'Debes seleccionar un role',
                  },
                ]}
              >
                <Select placeholder='Role de usuario'>
                  <Select.Option value='A'>Administrador</Select.Option>
                  <Select.Option value='C'>Conductor</Select.Option>
                </Select>
              </Form.Item>
            ) : null}
            <Form.Item>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button
                  onClick={this.closeModal}
                  loading={this.state.loading}
                  style={{ margin: '5px' }}
                >
                  Cancelar
                </Button>
                <Button
                  type='primary'
                  htmlType='submit'
                  loading={this.state.loading}
                  style={{ margin: '5px' }}
                >
                  Guardar
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }
}

export default UserComponent;
