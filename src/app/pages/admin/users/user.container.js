import {
  connect
} from 'react-redux';
import UserComponent from './user.component';

const mapStateToProps = state => ({
  commerce: state.auth.commerce,
  role: state.auth.role
});

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserComponent);