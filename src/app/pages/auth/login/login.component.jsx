import React, { Component } from 'react';
import { Form, Layout, Card, Input, Button } from 'antd';
import {
  FacebookFilled,
  InstagramFilled,
  TwitterCircleFilled,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import UserProvider from '../../../providers/user.provider';
import notifications from '../../../components/notifications/notifications';
import Logo from '../../../../assets/svg/fattorinisvg.svg';
import './login.component.scss';

const { Content } = Layout;

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false,
    };
  }
  onChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value });
  };
  onFinish = (values) => {
    this.setState({ loading: true });
    console.log('Received values of form: ', values);
    UserProvider.login(values).then(
      (res) => {
        console.log(res.data);
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('role', res.data.role);
        this.setState({ loading: false });
        window.location.reload();
      },
      (err) => {
        this.setState({ loading: false });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }} className='login'>
        <Content
          style={{
            justifyContent: 'space-around',
            alignItems: 'center',
            display: 'flex',
            padding: '60px',
          }}
        >
          <div className='col-sm-3 socials'>
            <img src={Logo} alt='Logo fattorini'></img>
            <h1>Bienvenido!</h1>
            <p>Vende tus productos y nosotros nos encargamos de la entrega!.</p>
            <div className='icons'>
              <FacebookFilled />
              <InstagramFilled />
              <TwitterCircleFilled />
            </div>
          </div>
          <div className='col-sm-3'>
            <Card bordered={false}>
              <h3 className='title'>Log in</h3>
              <Form
                name='normal_login'
                className='login-form'
                onFinish={this.onFinish}
              >
                <Form.Item
                  name='email'
                  rules={[
                    {
                      type: 'email',
                      message: 'Debes introducir un email valido',
                    },
                    {
                      required: true,
                      message: 'Debes ingresar un correo de usuario',
                    },
                  ]}
                >
                  <Input size='large' placeholder='Correo' />
                </Form.Item>
                <Form.Item
                  name='password'
                  rules={[
                    {
                      required: true,
                      message: 'Debes ingresar una contraseña',
                    },
                  ]}
                >
                  <Input.Password
                    size='large'
                    type='password'
                    placeholder='Contraseña'
                  />
                </Form.Item>
                <Form.Item>
                  <Button
                    size='large'
                    type='primary'
                    htmlType='submit'
                    className='login-form-button'
                    loading={this.state.loading}
                  >
                    Ingresar
                  </Button>
                </Form.Item>
              </Form>
              <div className='recovery-pass'>
                <Link to='/recovery-pass'>Olvide mi contraseña</Link>
              </div>
            </Card>
            <div className='register-option'>
              <div>¿Tienes un comercio?</div>
              <Link to='/singup'>registrate ahora</Link>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default LoginComponent;
