import React, { Component } from 'react';
import { Form, Layout, Card, Input, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import './recoverypass.component.scss';
import { Link } from 'react-router-dom';

const { Content } = Layout;

class RecoveryPassComponent extends Component {
  onFinish = (values) => {
    console.log('Received values of form: ', values);
  };
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }} className='recovery'>
        <Content
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
          }}
        >
          <Card bordered={false} className='col-sm-3'>
            <h3 className='title'>Recuperar contraseña</h3>
            <p>
              Recibiras en tu correo electrónico los pasos a seguir para
              recuperar tu contraseña
            </p>
            <Form
              name='normal_login'
              className='login-form'
              initialValues={{ remember: true }}
              onFinish={this.onFinish}
            >
              <Form.Item
                name='email'
                rules={[
                  { required: true, message: 'Please input your Username!' },
                ]}
              >
                <Input
                  size='large'
                  prefix={<UserOutlined className='site-form-item-icon' />}
                  placeholder='Correo'
                />
              </Form.Item>
              <Form.Item>
                <Button
                  size='large'
                  type='primary'
                  htmlType='submit'
                  className='login-form-button'
                >
                  Recuperar mi contraseña
                </Button>
              </Form.Item>
            </Form>
            <div className='register-option'>
              ¿No tienes una cuenta? <Link to='/singup'>registrate ahora</Link>
            </div>
            <div className='register-option'>
              <Link to='/login'>Ya tengo una cuenta</Link>
            </div>
          </Card>
        </Content>
      </Layout>
    );
  }
}

export default RecoveryPassComponent;
