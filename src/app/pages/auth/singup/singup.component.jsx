import React, { Component } from 'react';
import { Form, Layout, Card, Input, Button, Upload, Select } from 'antd';
import './singup.component.scss';
import { Link } from 'react-router-dom';

import { UploadOutlined } from '@ant-design/icons';
import CommerceProvider from '../../../providers/commerce.provider';
import UserProvider from '../../../providers/user.provider';
import notifications from '../../../components/notifications/notifications';
import api from '../../../providers/api';
import { venezuela } from '../../../providers/venezuela';
const { Content } = Layout;

class SingUpComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      page: 0,
      commerce: null,
      user: null,
      fileList: [],
      cities: venezuela[0].ciudades,
    };
  }
  onFinish = (value) => {
    let dataCommerce = {
      name: value.commerce,
    };
    this.setState({ loading: true });
    let dataUser = {
      firstname: value.firstname,
      password: value.password,
      email: value.email,
      role: 'CA',
    };
    UserProvider.UserExist({ email: dataUser.email }).then(
      (result) => {
        if (result.data) {
          CommerceProvider.SaveCommerce(dataCommerce).then(
            (res) => {
              if (res.data) {
                this.setState({ commerce: res.data });
                UserProvider.RegisterCA(dataUser, res.data._id).then(
                  (userres) => {
                    if (userres.data) {
                      this.setState({
                        loading: false,
                        page: 1,
                        user: userres.data.user,
                      });
                      localStorage.setItem('token', userres.data.token);
                      localStorage.setItem('role', userres.data.role);
                      localStorage.setItem('commerce', res.data._id);
                    }
                  },
                  (err) => {
                    this.setState({
                      loading: false,
                    });
                    if (err.data)
                      notifications.showNotification('error', err.data.error);
                    else
                      notifications.showNotification(
                        'error',
                        'Error de conexión',
                        'Por favor intentelo más tarde'
                      );
                  }
                );
              }
            },
            (err) => {
              this.setState({
                loading: false,
              });
              if (err.data)
                notifications.showNotification('error', err.data.error);
              else
                notifications.showNotification(
                  'error',
                  'Error de conexión',
                  'Por favor intentelo más tarde'
                );
            }
          );
        }
      },
      (err) => {
        this.setState({
          loading: false,
        });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  onComplete = (value) => {
    if (this.state.fileList.length <= 0) {
      notifications.showNotification(
        'error',
        'Debe ingresar una imágen para el perfíl del comercio'
      );
      return;
    }
    let data = {
      picture: this.state.fileList[0].response.url,
      rif: value.rif,
      address: value.address,
      city: value.city,
      state: value.state,
      description: value.description,
    };
    this.setState({ loading: true });
    CommerceProvider.UpdateCommerce(
      localStorage.getItem('commerce'),
      data
    ).then(
      (res) => {
        if (res.data) {
          this.setState({
            loading: false,
          });
          window.location.reload();
        }
      },
      (err) => {
        this.setState({
          loading: false,
        });
        if (err.data) notifications.showNotification('error', err.data.error);
        else
          notifications.showNotification(
            'error',
            'Error de conexión',
            'Por favor intentelo más tarde'
          );
      }
    );
  };
  changePage = () => {
    window.locationd.reload();
  };
  setCities = (estado) => {
    for (let index = 0; index < venezuela.length; index++) {
      const est = venezuela[index];
      if (est.estado === estado) {
        this.setState({ cities: est.ciudades });
        return;
      }
    }
  };
  handleChange = ({ fileList }) => {
    console.log(fileList);
    this.setState({ fileList });
  };
  render() {
    const props = {
      action: api.image + '/file/upload',
      listType: 'picture',
      fileList: this.state.fileList,
      onChange: this.handleChange,
    };
    return (
      <Layout style={{ minHeight: '100vh' }} className='singup'>
        <Content
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
          }}
        >
          {this.state.page === 0 ? (
            <div className='col-sm-3'>
              <Card bordered={false}>
                <h3 className='title'>Sing up</h3>
                <Form className='login-form' onFinish={this.onFinish}>
                  <Form.Item
                    name='firstname'
                    rules={[
                      { required: true, message: 'Debes indicar tu nombre' },
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || value.length >= 3) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            'Debes introducir un nombre valido'
                          );
                        },
                      }),
                    ]}
                  >
                    <Input size='large' placeholder='Tu nombre' />
                  </Form.Item>
                  <Form.Item
                    name='email'
                    rules={[
                      {
                        type: 'email',
                        message: 'Debes introducir un email valido',
                      },
                      {
                        required: true,
                        message: 'Debes indicar tu correo de usuario',
                      },
                    ]}
                  >
                    <Input size='large' placeholder='Correo' />
                  </Form.Item>
                  <Form.Item
                    name='password'
                    rules={[
                      {
                        required: true,
                        message: 'Debes registrar una contraseña',
                      },
                    ]}
                  >
                    <Input.Password
                      size='large'
                      type='password'
                      placeholder='Contraseña'
                    />
                  </Form.Item>
                  <Form.Item
                    name='passwordconfirm'
                    rules={[
                      {
                        required: true,
                        message: 'Debes confirmar tu contraseña',
                      },
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || getFieldValue('password') === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject('Las contraseñas no coinciden');
                        },
                      }),
                    ]}
                  >
                    <Input
                      size='large'
                      type='password'
                      placeholder='Confirmar contraseña'
                    />
                  </Form.Item>
                  <Form.Item
                    name='commerce'
                    rules={[
                      {
                        required: true,
                        message:
                          'Debes indicar el nombre de tu comercio/negocio',
                      },
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || value.length >= 3) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            'Debes introducir un nombre válido'
                          );
                        },
                      }),
                    ]}
                  >
                    <Input
                      size='large'
                      placeholder='Nombre del comercio/negocio'
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button
                      size='large'
                      type='primary'
                      htmlType='submit'
                      className='login-form-button'
                      loading={this.state.loading}
                    >
                      Registrarme
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
              <div className='register-option'>
                <Link to='/login'>Ya tengo una cuenta</Link>
              </div>
            </div>
          ) : (
            <div className='col-sm-6'>
              <h1>hola {this.state.user.firstname}!</h1>
              <p>
                para terminar de registrar <b>{this.state.commerce.name}</b>,
                debemos tener algunos datos extras.
              </p>
              <Card bordered={false}>
                <Form className='login-form' onFinish={this.onComplete}>
                  <Form.Item>
                    <Upload {...props}>
                      {this.state.fileList.length <= 0 ? (
                        <Button>
                          <UploadOutlined /> Imagen del perfil del comercio
                        </Button>
                      ) : null}
                    </Upload>
                  </Form.Item>
                  <Form.Item
                    name='rif'
                    rules={[
                      {
                        required: true,
                        message:
                          'Debes indicar el rif o identificación del comercio',
                      },
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || value.length >= 9) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            'Debes introducir un rif válido'
                          );
                        },
                      }),
                    ]}
                  >
                    <Input
                      size='large'
                      placeholder='Indica el rif de tu comercio'
                    />
                  </Form.Item>
                  <Form.Item name='description'>
                    <Input.TextArea
                      size='large'
                      placeholder='Describe tu comercio'
                    />
                  </Form.Item>
                  <Form.Item
                    name='state'
                    rules={[
                      {
                        required: true,
                        message: 'Debes seleccionar un estado',
                      },
                    ]}
                  >
                    <Select
                      defaultValue={venezuela[0].estado}
                      placeholder='Estado'
                      onChange={this.setCities}
                    >
                      {venezuela.map((estado) => (
                        <Select.Option value={estado.estado}>
                          {estado.estado}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name='city'
                    rules={[
                      {
                        required: true,
                        message: 'Debes seleccionar una ciudad',
                      },
                    ]}
                  >
                    <Select
                      defaultValue={this.state.cities[0]}
                      placeholder='Ciudad'
                    >
                      {this.state.cities.map((city) => (
                        <Select.Option value={city}>{city}</Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item name='address'>
                    <Input size='large' placeholder='Dirección' />
                  </Form.Item>

                  <Form.Item>
                    <div style={{ display: 'flex' }}>
                      {/*                       <Button
                        size='large'
                        type='seccundary'
                        className='login-form-button'
                        loading={this.state.loading}
                        onClick={this.changePage}
                        style={{ margin: '0 5px' }}
                      >
                        Omitir
                      </Button> */}
                      <Button
                        size='large'
                        type='primary'
                        htmlType='submit'
                        className='login-form-button'
                        loading={this.state.loading}
                        style={{ margin: '0 5px' }}
                      >
                        Listo
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </Card>
            </div>
          )}
        </Content>
      </Layout>
    );
  }
}

export default SingUpComponent;
