import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import LoginComponent from './auth/login/login.component';
import SingUpComponent from './auth/singup/singup.component';
import AdminComponent from './admin/admin.component';
import RecoveryPassComponent from './auth/recoverypass/recoverypass.component';
import userProvider from '../providers/user.provider';
import notifications from '../components/notifications/notifications';
import commerceProvider from '../providers/commerce.provider';

class PagesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
    };
  }
  componentDidMount() {
    this.getDatas();
  }
  getDatas = () => {
    let token = this.getToken();
    let role = this.getRole();
    if (token !== '' && token) {
      this.setState({ token: this.getToken() });
      /* Guardamos el token y role en el store */
      this.props.setDateAuth({ token, role });

      /* Buscamos los datos del usuario logeado */
      userProvider.UserLoggedin(token).then(
        (res) => {
          /* Guardamos el usuario en el store */
          this.props.setDataUser(res.data);
          /* Verificamos si aes administrador de comercio */
          if (role === 'CA') {
            let commerce = localStorage.getItem('commerce');
            /* si no se guardo en el storage el id del comercio */
            if (commerce !== '') {
              commerceProvider.GetCommerceByAdmin(res.data._id).then(
                (response) => {
                  /* Lo guardamos en el store */
                  this.props.setDataCommerce(response.data.commerce);
                },
                (err) => {
                  this.setState({ loading: false });
                  if (err.data)
                    notifications.showNotification('error', err.data.error);
                  else
                    notifications.showNotification(
                      'error',
                      'Error de conexión',
                      'Por favor intentelo más tarde'
                    );
                }
              );
            } else {
              /* Buscamos el comercio */
              commerceProvider.GetCommerce(commerce).then(
                (response) => {
                  /* Lo guardamos en el store */
                  this.props.setDataCommerce(response.data);
                },
                (err) => {
                  this.setState({ loading: false });
                  if (err.data)
                    notifications.showNotification('error', err.data.error);
                  else
                    notifications.showNotification(
                      'error',
                      'Error de conexión',
                      'Por favor intentelo más tarde'
                    );
                }
              );
            }
          }
        },
        (err) => {
          this.setState({ loading: false });
          localStorage.clear();
          if (err.data) notifications.showNotification('error', err.data.error);
          else
            notifications.showNotification(
              'error',
              'Error de conexión',
              'Por favor intentelo más tarde'
            );
          window.location.reload();
        }
      );
    }
  };
  getToken = () => {
    let token = localStorage.getItem('token');
    return token;
  };
  getRole = () => {
    let role = localStorage.getItem('role');
    return role;
  };
  render() {
    return (
      <Router>
        {!this.state.token ? (
          <>
            <Route path='/login' component={LoginComponent} />
            <Route path='/singup' component={SingUpComponent} />
            <Route path='/recovery-pass' component={RecoveryPassComponent} />
          </>
        ) : (
          <Route path='/admin' component={AdminComponent} />
        )}
        <Route exact path='/'>
          {!this.state.token ? (
            <Redirect to='/login' />
          ) : (
            <Redirect to='/admin/dashboard' />
          )}
        </Route>
        <Route exact path='*'>
          {!this.state.token ? (
            <Redirect to='/login' />
          ) : (
            <Redirect to='/admin/dashboard' />
          )}
        </Route>
      </Router>
    );
  }
}

export default PagesComponent;
