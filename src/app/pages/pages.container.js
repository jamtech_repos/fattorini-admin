import {
  connect
} from 'react-redux';
import PagesComponent from './pages.component';
import types from '../../redux/types';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  setDateAuth(data) {
    dispatch({
      type: types.SET_DATA_AUTH,
      role: data.role,
      token: data.token
    })
  },
  setDataCommerce(data) {
    dispatch({
      type: types.SET_DATA_COMMERCE,
      commerce: data
    })
  },
  setDataUser(data) {
    dispatch({
      type: types.SET_DATA_USER,
      user: data
    })
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PagesComponent);