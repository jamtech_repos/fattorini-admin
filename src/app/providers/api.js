const api = {
  url: 'https://fattorini-api.herokuapp.com/api',
  image: 'http://appfattorini.com/fileapi'
};

export default api;