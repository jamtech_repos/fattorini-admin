import Api from './api'
import axios from 'axios'

const GetCategory = async () => {
  return await axios.get(Api.url + '/category/list').catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const SaveCategory = async (data) => {
  return await axios.post(Api.url + '/category', data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const UpdateAllCategories = async (data) => {
  return await axios.put(Api.url + '/category/all', data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const UpdateCategory = async (data, id) => {
  return await axios.put(Api.url + '/category?id=' + id, data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const DeleteCategory = async (id) => {
  return await axios.delete(Api.url + '/category?id=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
export default {
  GetCategory,
  SaveCategory,
  UpdateAllCategories,
  DeleteCategory,
  UpdateCategory
}