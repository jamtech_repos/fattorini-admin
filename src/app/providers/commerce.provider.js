import Api from './api'
import axios from 'axios'

const GetCommerces = async () => {
  return await axios.get(Api.url + '/commerce/list').catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const GetCommerce = async (id) => {
  return await axios.get(Api.url + '/commerce?id=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const SaveCommerce = async (data) => {
  return await axios.post(Api.url + '/commerce', data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const UpdateCommerce = async (id, data) => {
  return await axios.put(Api.url + '/commerce?id=' + id, data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const DeleteCommerce = async (id) => {
  return await axios.delete(Api.url + '/commerce?id=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const GetCommerceByAdmin = async (id) => {
  return await axios.get(Api.url + '/admins/commerce?user=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const ListAdmins = async (id) => {
  return await axios.get(Api.url + '/admins/list?commerce=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
export default {
  GetCommerces,
  GetCommerce,
  SaveCommerce,
  UpdateCommerce,
  DeleteCommerce,
  GetCommerceByAdmin,
  ListAdmins
}