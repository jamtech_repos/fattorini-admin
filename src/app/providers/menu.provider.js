import Api from './api'
import axios from 'axios'

const GetMenu = async (commerce) => {
  return await axios.get(Api.url + '/menu/list?commerce=' + commerce).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const SaveMenu = async (data) => {
  return await axios.post(Api.url + '/menu', data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const UpdateAllMenu = async (data) => {
  return await axios.put(Api.url + '/menu/all', data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const UpdateMenu = async (data, id) => {
  return await axios.put(Api.url + '/menu?id=' + id, data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const DeleteMenu = async (id) => {
  return await axios.delete(Api.url + '/menu?id=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
export default {
  GetMenu,
  SaveMenu,
  UpdateAllMenu,
  DeleteMenu,
  UpdateMenu
}