import Api from './api'
import axios from 'axios'

const GetByCommerce = async (commerce) => {
  return await axios.get(Api.url + '/product/commerce?commerce=' + commerce).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const SaveProduct = async (data) => {
  return await axios.post(Api.url + '/product', data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const UpdateProduct = async (id, data) => {
  return await axios.put(Api.url + '/product?id=' + id, data).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}
const DeleteProduct = async (id) => {
  return await axios.delete(Api.url + '/product?id=' + id).catch(err => {
    if (err.response) {
      throw (err.response)
    } else if (err.request) {
      throw (err.request)
    } else throw (err)
  })
}

export default {
  GetByCommerce,
  SaveProduct,
  DeleteProduct,
  UpdateProduct
}