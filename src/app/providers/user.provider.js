import Api from './api'
import axios from 'axios'

const login = async (data) => {
  return await axios.post(Api.url + '/user/login', data).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}
const UpdateUser = async (id, data) => {
  return await axios.put(Api.url + '/user/update?id=' + id, data).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}
const UserExist = async (data) => {
  return await axios.post(Api.url + '/user/exist', data).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}
const RegisterCA = async (data, commerce) => {
  return await axios.post(Api.url + '/user/register/CA/' + commerce, data).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}
const SaveUser = async (data) => {
  return await axios.post(Api.url + '/user/register', data).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}
const UserLoggedin = async (token) => {
  return await axios.get(Api.url + '/user/loggedin', {
    headers: {
      authorization: token
    }
  }).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}

const ListUserByRole = async (role) => {
  return await axios.get(Api.url + '/user/list?role=' + role, {

  }).catch(err => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
      throw (err.response)
    } else if (err.request) {
      // client never received a response, or request never left
      throw (err.request)
    } else {
      // anything else
      throw (err)
    }
  })
}
export default {
  login,
  RegisterCA,
  UserExist,
  UserLoggedin,
  UpdateUser,
  ListUserByRole,
  SaveUser
}