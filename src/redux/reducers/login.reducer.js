import types from '../types';
import update from 'react-addons-update';

const INITIAL_STATE = {
  user: { firtsname: '' },
  commerce: { name: '' },
  role: null,
  token: null,
};
const LoginReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SET_DATA_AUTH: {
      return update(state, {
        role: { $set: action.role },
        token: { $set: action.token },
      });
    }
    case types.SET_DATA_USER: {
      return update(state, {
        user: { $set: action.user },
      });
    }
    case types.SET_DATA_COMMERCE: {
      console.log(action);
      return update(state, {
        commerce: { $set: action.commerce },
      });
    }
    default:
      return state;
  }
};

export default LoginReducer;
