import {
  combineReducers
} from 'redux';
import LoginReducer from './reducers/login.reducer'

const rootReducer = combineReducers({
  auth: LoginReducer
});

export default rootReducer;